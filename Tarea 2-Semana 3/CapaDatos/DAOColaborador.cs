﻿using CapaObjetos;
using Npgsql;
using System.Collections.Generic;

namespace CapaDatos
{
    public class DAOColaborador
    {
        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;
        Colaborador colaborador;

        public void InsertarDatos(Colaborador col)
        {
            conexion = Conexion.conexionBD();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO colaborador(cedula,nombre,genero,id_departamento,fecha_nacimiento,edad,fecha_ingreso,nivel_ingles,id_puesto ) VALUES " +
                "('" + col.cedula + "', '" + col.nombre + "', '" + col.genero + "', '" + col.id_departamento + "', '" + col.fecha_nac + "', '" + col.edad + "', '" + col.fecha_ingreso + "', '" + col.nivel_ingles + "', '" + col.id_puesto + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public List<Colaborador> ConsultarDatos(int cedula)
        {
            List<Colaborador> lista = new List<Colaborador>();
            conexion = Conexion.conexionBD();
            conexion.Open();
            cmd = new NpgsqlCommand("SELECT cedula,nombre,genero,id_departamento,fecha_nacimiento,edad,fecha_ingreso,nivel_ingles,id_puesto FROM colaborador WHERE cedula = '" + cedula + "';", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    colaborador = new Colaborador
                    {
                        cedula = dr.GetInt32(0),
                        nombre = dr.GetString(1),
                        genero = dr.GetString(2),
                        id_departamento = dr.GetInt32(3),
                        fecha_nac = dr.GetDateTime(4),
                        edad = dr.GetInt32(5),
                        fecha_ingreso = dr.GetDateTime(6),
                        nivel_ingles = dr.GetString(7),
                        id_puesto = dr.GetInt32(8)
                    };
                    lista.Add(colaborador);
                }
            }
            conexion.Close();
            return lista;
        }
    }
}
