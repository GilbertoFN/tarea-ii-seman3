﻿using CapaObjetos;
using CapaDatos;
using System.Collections.Generic;

namespace CapaNegocio
{
    public class BOColaborador
    {
        public void Registrar(Colaborador col)
        {
            new DAOColaborador().InsertarDatos(col);
        }

        public List<Colaborador> CargarDatos(int cedula)
        {
            return new DAOColaborador().ConsultarDatos(cedula);
        }
    }
}
