﻿using System.Windows.Forms;
using System;
using CapaObjetos;
using CapaNegocio;
using System.Collections.Generic;

namespace CapaPresentacion
{
    public partial class FrmColaboradores : Form
    {
        List<Colaborador> lista;
        public BOColaborador boCol;
        public FrmColaboradores()
        {
            InitializeComponent();
            boCol = new BOColaborador();
        }

        private void btnLeer_Click(object sender, EventArgs e)
        {
            //int index = Convert.ToInt32(txtCedula.Text);
            List<Colaborador> lista = boCol.CargarDatos(123);
            txtNombre.Text = lista[0].nombre;
            if (lista[0].genero == "Masculino")
            {
                rbMasculino.Checked = true;
                rbFemenino.Checked = false;
            }
            else
            {
                rbMasculino.Checked = false;
                rbFemenino.Checked = true;
            }
            cbDepartamento.Text = "EN PROCESO";
            dtpFecha_Nac.Text = lista[0].fecha_nac.ToString();
            txtEdad.Text = lista[0].edad.ToString();
            dtpFecha_Ingreso.Text = lista[0].fecha_ingreso.ToString();
            cbNivel_Ingles.Text = "EN PROCESO";
            cbPuesto.Text = "EN PROCESO";
        }
    }
    
}
