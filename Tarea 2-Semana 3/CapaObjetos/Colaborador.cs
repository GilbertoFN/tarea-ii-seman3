﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaObjetos
{
    /// <summary>
    /// Objeto tipo estudiante
    /// </summary>
    public class Colaborador
    {
        public int cedula { get; set; }
        public string nombre { get; set; }
        public string genero { get; set; }
        public int id_departamento { get; set; }
        public DateTime fecha_nac { get; set; }
        public int edad { get; set; }
        public DateTime fecha_ingreso { get; set; }
        public string nivel_ingles { get; set; }
        public int id_puesto { get; set; }
    }
}
